#!/bin/bash

set -xe

IN_DIR="$1"
OUT_FILE="$2"
TARGET_OUT_DIR=${IN_DIR}/../../../out/odroid-c2

if [ -d "$IN_DIR" ]; then
	dd if=${IN_DIR}/bl1.bin.hardkernel of=$OUT_FILE conv=fsync,notrunc bs=1 count=442
	dd if=${IN_DIR}/bl1.bin.hardkernel of=$OUT_FILE conv=fsync,notrunc bs=512 skip=1 seek=1

	${IN_DIR}/../tools/fip_create \
		--bl30 ${IN_DIR}/bl30.bin \
		--bl301 ${IN_DIR}/bl301.bin \
		--bl31 ${IN_DIR}/bl31.bin \
		--bl33 ${TARGET_OUT_DIR}/build/uboot/u-boot.bin \
			${TARGET_OUT_DIR}/build/uboot/fip.bin

	${IN_DIR}/../tools/fip_create --dump ${TARGET_OUT_DIR}/build/uboot/fip.bin

	cat ${IN_DIR}/bl2.package ${TARGET_OUT_DIR}/build/uboot/fip.bin \
		> ${TARGET_OUT_DIR}/build/uboot/uboot.fip.bin

	${IN_DIR}/../tools/aml_encrypt_gxb --bootsig --input \
		${TARGET_OUT_DIR}/build/uboot/uboot.fip.bin \
			--output ${TARGET_OUT_DIR}/build/uboot/uboot.aml.bin.tmp

	dd if=${TARGET_OUT_DIR}/build/uboot/uboot.aml.bin.tmp \
		of=${TARGET_OUT_DIR}/build/uboot/uboot.aml.bin bs=512 skip=96

	dd if=${TARGET_OUT_DIR}/build/uboot/uboot.aml.bin \
		of=$OUT_FILE conv=fsync,notrunc bs=512 seek=97
fi
